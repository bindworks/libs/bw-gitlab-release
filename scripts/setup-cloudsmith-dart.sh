#!/usr/bin/env bash

# Usage: setup-cloudsmith-dart.sh <environment-variable-name-with-cloudsmith-token> <cloudsmith-repository> 
#
# <cloudsmith-repository> defaults to https://dart.cloudsmith.io/bindworks/dart/
#

REPOSITORY_URL="${2:-https://dart.cloudsmith.io/bindworks/dart/}"

if [ "$1" = "" ]; then
    echo "❗ Usage: $0 <environment-variable-name-with-cloudsmith-token>" >&2
    exit 1
fi

export ACCESS_TOKEN="${!1}"

if [ "$ACCESS_TOKEN" = "" ]; then
    echo "❗ Variable $1 does not contain any cloudsmith access token, skipping the auth" >&2
    exit 0
fi

echo "🌟 Setting up cloudsmith repository $REPOSITORY_URL..." >&2
dart pub token add --env-var "$1" "$REPOSITORY_URL"
