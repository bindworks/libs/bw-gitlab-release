#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )";
source "$SCRIPT_DIR/common.inc.sh"

TARGET_BRANCH="$(cicd_find_target_branch)"
BUMP="$(cicd_determine_bump)" || { echo "🧑‍🏭 Changelog does not contain BUMP instructions, not releasing."; exit 0; }

cicd_step_title "Version bump: $BUMP"
cider bump $BUMP
git add pubspec.yaml
VERSION="$(cider version)"

cicd_step_title "Release changelog"
cicd_release_changelog "$VERSION"
git add CHANGELOG.md

cicd_step_title "Commit back to branch $TARGET_BRANCH"
cicd_git_commit_and_push_to_target_branch "$VERSION"
cicd_git_tag_version_and_push "$VERSION"

cicd_step_title "Publish to private Dart repository"
skip_on_dryrun git rm -rq example || true
skip_on_dryrun rm -rf example
skip_on_dryrun dart pub publish -f
